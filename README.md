# gt.TaskScheduler

#### 介绍
分布式任务调度组件。提供集群任务调度、分布式任务执行功能

#### 软件架构

##### 角色定义


1. 普通节点。负责消费队列中的Task，并进行相关的逻辑处理。并支持多线程并行消费。
2. Leader节点。除了普通节点的功能外，还负责维护整个集群的运行状态维护，以及初始话队列数据等工作。
3. 消费队列。维护集群的所有Task，支持生产者消费者模式。当前很多方案可以选型。例如 Redis、Zookeeper。
4. 队列数据。由{dbname}.{tablename}构成，这样每个task即使代表一张具体的表。


##### 集群流程：

1. 集群节点同时启动（这里会依赖QuartzJob的时间启动机制），竞争选举一个节点为Leader节点。一个集群只能有一个节点
2. Leader节点进行队列数据及集群状态等初始化工作。普通节点等待集群状态变更到提示可以开始工作。
3. 集群节点（包括Leader节点）作为消费者，从队列拉取task，获取待处理的{dbname}.{tablename}。批量拉取数据处理存入缓存。如果队列空，则更新本节点的执行状态
4. Leader节点完成数据逻辑工作，开始监控集群其他节点的状态，如果所有节点状态都已经是 已完成停止 状态，则开始集群停止相关状态更新工作。
5. 整个任务完成。等待下一次集群工作。
6. 过程中，如果有新节点加入，会自动以普通节点加入集群工作。

##### 集群状态，以KEY-VALUE方式存储在分布式组件中。以Redis为例

1. 集群标识：ts:{taskSchedulerName}。taskSchedulerName为每个集群任务的名称。
2. 集群节点：ts:{taskSchedulerName}:members。hash结构存储，field为 节点名称，value标识 节点状态
3. 集群Leader：ts:{taskSchedulerName}:leader。hash结构存储，field为 Leader节点名称，value标识 集群状态
3. 集群队列：ts:{taskSchedulerName}:q。List结构存储。value为待处理的taskname，此例中为 表的全名
4. 分布式锁：ts:{taskSchedulerName}:lock。用于分布式锁的Key值

#### 使用说明

启动方式

```
var schedulerName = "build_test";

using (var distribute = new RedisDistributeFeature(_redisConn, 3, 10))
{
    TaskSchedulerManagerBuilder builder = new TaskSchedulerManagerBuilder(schedulerName, distribute);
    builder.AddTaskHandler("sample1", new SampleHandler(distribute));
    builder.AddTaskHandler("sample2", new SampleHandler(distribute));
    var taskScheduler = builder.Build();
    taskScheduler.Run();

    Assert.Equal("0", distribute.GetData(_sampleKey, "sample1"));
    Assert.Equal("0", distribute.GetData(_sampleKey, "sample2"));
	distribute.DeleteData(_sampleKey);
}
```			


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
