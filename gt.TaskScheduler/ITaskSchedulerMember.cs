﻿/**************************************************************
 * Copyright gt1987. All rights reserved.
 * 
 * Author: guitao(guitao@eastmoney.com) 
 * Create Date: 2020/5/8 20:31:16
 * Description: ITaskSchedulerManager
 *          
 * Revision History:
 *      Date         Author               Description
 *              
***************************************************************/

using gt.TaskScheduler.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace gt.TaskScheduler
{
    /// <summary>
    /// 集群任务调度管理器
    /// </summary>
    public interface ITaskSchedulerMember
    {
        /// <summary>
        /// 处理集群事务
        /// </summary>
        void Run();
        /// <summary>
        /// 设置为主节点
        /// </summary>
        /// <param name="leader"></param>
        void SetAsLeader(ITaskSchedulerLeader leader);
    }
}
