﻿/**************************************************************
 * Copyright gt1987. All rights reserved.
 * 
 * Author: guitao(guitao@eastmoney.com) 
 * Create Date: 2020/5/12 13:23:26
 * Description: ITaskSchedulerMananger
 *          
 * Revision History:
 *      Date         Author               Description
 *              
***************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace gt.TaskScheduler.Core
{
    /// <summary>
    /// 任务调度器 管理接口
    /// </summary>
    public interface ITaskSchedulerMananger
    {
        /// <summary>
        /// 节点member注册
        /// </summary>
        void RegisterMember(string memberName);
        /// <summary>
        /// 注册集群Leader
        /// </summary>
        void RegisterLeader(string memberName);
        /// <summary>
        /// 初始化任务列表
        /// </summary>
        /// <returns></returns>
        void InitTaskQueue(List<string> taskNames);
        /// <summary>
        /// 标记任务调度集群状态
        /// </summary>
        /// <param name="memberName">节点名称</param>
        /// <param name="status">状态</param>
        void MarkTaskSchedulerStatus(string memberName, TaskSchedulerStatus status);
        /// <summary>
        /// 标记节点状态
        /// </summary>
        /// <param name="memberName">节点名称</param>
        /// <param name="status">状态</param>
        void MarkMemberStatus(string memberName, MemberStatus status);
        /// <summary>
        /// 拉取TaskName
        /// </summary>
        /// <returns>返回 null 则表示队列已消费完</returns>
        string DequeueTaskName();
        /// <summary>
        /// 检查任务调度集群是否可以重新开始
        /// </summary>
        /// <returns></returns>
        bool CheckTaskSchedulerCanStart();
        /// <summary>
        /// 检查任务调度集群是否开始运行
        /// </summary>
        /// <returns></returns>
        bool CheckTaskSchedulerRunning();
        /// <summary>
        /// 检查集群所有节点是否完成
        /// </summary>
        /// <returns></returns>
        bool CheckAllMembersFinished();
    }
}
