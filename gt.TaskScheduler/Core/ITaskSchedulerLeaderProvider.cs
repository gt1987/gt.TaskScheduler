﻿/**************************************************************
 * Copyright gt1987. All rights reserved.
 * 
 * Author: guitao(guitao@eastmoney.com) 
 * Create Date: 2020/5/11 16:47:38
 * Description: ITaskSchedulerLeaderProvider
 *          
 * Revision History:
 *      Date         Author               Description
 *              
***************************************************************/

using gt.TaskScheduler.Core.Components;
using System;
using System.Collections.Generic;
using System.Text;

namespace gt.TaskScheduler.Core
{
    public interface ITaskSchedulerLeaderProvider
    {
        /// <summary>
        /// 尝试生成任务调度Leader。
        /// 一个集群只能有1个Leader
        /// </summary>
        /// <param name="memberName">集群任务调度名称</param>
        /// <returns>非空，则表示当前member为Leader。</returns>
        ITaskSchedulerLeader TryCreateLeader(string memberName, ITaskSchedulerMananger manager);
    }
}
