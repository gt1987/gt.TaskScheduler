﻿/**************************************************************
 * Copyright gt1987. All rights reserved.
 * 
 * Author: guitao(guitao@eastmoney.com) 
 * Create Date: 2020/5/8 20:34:05
 * Description: ITaskSchedulerHandler
 *          
 * Revision History:
 *      Date         Author               Description
 *              
***************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace gt.TaskScheduler.Core
{
    /// <summary>
    /// 任务处理Handler
    /// </summary>
    public interface ITaskHandler
    {
        ///// <summary>
        ///// 可以处理的schedule列表
        ///// </summary>
        //List<string> scheduleNames { get; }
        /// <summary>
        /// 处理任务
        /// </summary>
        Task Execute(string tag);
    }
}
