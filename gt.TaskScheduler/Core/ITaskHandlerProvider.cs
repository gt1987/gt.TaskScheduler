﻿/**************************************************************
 * Copyright gt1987. All rights reserved.
 * 
 * Author: guitao(guitao@eastmoney.com) 
 * Create Date: 2020/5/9 14:49:31
 * Description: ITaskHandlerFactory
 *          
 * Revision History:
 *      Date         Author               Description
 *              
***************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace gt.TaskScheduler.Core
{
    public interface ITaskHandlerProvider
    {
        /// <summary>
        /// 新增任务
        /// </summary>
        /// <param name="taskName">任务标识，应具体唯一性</param>
        /// <param name="handler">任务处理器</param>
        void Create(string taskName, ITaskHandler handler);
        /// <summary>
        /// 新增任务
        /// </summary>
        /// <param name="taskNames">任务标识列表。任务标识，应具体唯一性</param>
        void Create(List<string> taskNames, ITaskHandler handler);
        /// <summary>
        /// 获取任务处理器
        /// </summary>
        /// <param name="taskName"></param>
        /// <returns></returns>
        ITaskHandler GetHandler(string taskName);
        /// <summary>
        /// 获取所有TaskNames
        /// </summary>
        /// <returns></returns>
        List<string> GetAllTaskNames();
    }
}
