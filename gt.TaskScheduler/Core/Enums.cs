﻿/**************************************************************
 * Copyright gt1987. All rights reserved.
 * 
 * Author: guitao(guitao@eastmoney.com) 
 * Create Date: 2020/5/10 17:55:14
 * Description: Enums
 *          
 * Revision History:
 *      Date         Author               Description
 *              
***************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace gt.TaskScheduler.Core
{
    /// <summary>
    /// 节点状态
    /// </summary>
    public enum MemberStatus
    {
        /// <summary>
        /// 已注册
        /// </summary>
        Register = 1,
        /// <summary>
        /// 执行任务中
        /// </summary>
        Run = 2,
        /// <summary>
        /// 已停止
        /// </summary>
        Stop = 4
    }

    /// <summary>
    /// 集群
    /// </summary>
    public enum TaskSchedulerStatus
    {
        /// <summary>
        /// 集群启动
        /// </summary>
        Start=1,
        /// <summary>
        /// 运行中
        /// </summary>
        Run=2,
        /// <summary>
        /// 集群停止
        /// </summary>
        Stop=4
    }

}
