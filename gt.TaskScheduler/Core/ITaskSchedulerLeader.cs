﻿/**************************************************************
 * Copyright gt1987. All rights reserved.
 * 
 * Author: guitao(guitao@eastmoney.com) 
 * Create Date: 2020/5/11 14:22:30
 * Description: ITaskSchedulerLeader
 *          
 * Revision History:
 *      Date         Author               Description
 *              
***************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace gt.TaskScheduler.Core
{
    /// <summary>
    /// 集群任务调度器主节点
    /// TODO：如果Leader down机，如何更好的保持集群运行正常？
    /// </summary>
    public interface ITaskSchedulerLeader
    {
        /// <summary>
        /// 主节点初始化
        /// 集群状态设置
        /// 集群数据、队列数据等初始化
        /// </summary>
        /// <returns></returns>
        void Initialize(List<string> taskNames);
        /// <summary>
        /// 监控集群运行状态
        /// </summary>
        /// <returns></returns>
        Task MonitTaskSchedulerMembers();
    }
}
