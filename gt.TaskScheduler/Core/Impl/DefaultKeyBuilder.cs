﻿/**************************************************************
 * Copyright gt1987. All rights reserved.
 * 
 * Author: guitao(guitao@eastmoney.com) 
 * Create Date: 2020/5/10 15:33:58
 * Description: DefaultKeyBuilder
 *          
 * Revision History:
 *      Date         Author               Description
 *              
***************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace gt.TaskScheduler.Core.Impl
{
    public class DefaultKeyBuilder : IKeyBuilder
    {
        private readonly string _taskSchedulerName;
        private readonly string _memberCollectionKeySection = "members";
        private readonly string _memberLeaderKeySection = "leader";
        private readonly string _taskQueueKeySection = "q";
        private readonly string _taskSchedulerlockKeySection = "lock";

        public DefaultKeyBuilder(string taskSchedulerName)
        {
            _taskSchedulerName = string.Concat("ts:", taskSchedulerName);
        }

        public string GetMemberCollectionKey()
        {
            return string.Concat(_taskSchedulerName, ":", _memberCollectionKeySection);
        }

        public string GetMemberLeaderKey()
        {
            return string.Concat(_taskSchedulerName, ":", _memberLeaderKeySection);
        }

        public string GetTaskQueueKey()
        {
            return string.Concat(_taskSchedulerName, ":", _taskQueueKeySection);
        }

        public string GetLockKey()
        {
            return string.Concat(_taskSchedulerName, ":", _taskSchedulerlockKeySection);
        }
    }
}
