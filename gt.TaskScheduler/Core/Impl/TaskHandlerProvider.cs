﻿/**************************************************************
 * Copyright gt1987. All rights reserved.
 * 
 * Author: guitao(guitao@eastmoney.com) 
 * Create Date: 2020/5/9 13:20:19
 * Description: TaskSchedulerHandlerFactory
 *          
 * Revision History:
 *      Date         Author               Description
 *              
***************************************************************/

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gt.TaskScheduler.Core.Impl
{
    public class TaskHandlerProvider : ITaskHandlerProvider
    {
        private ConcurrentDictionary<string, ITaskHandler> _handlers = new ConcurrentDictionary<string, ITaskHandler>();

        /// <summary>
        /// 新增任务
        /// </summary>
        /// <param name="taskName">任务标识，应具体唯一性</param>
        /// <param name="handler">任务处理器</param>
        public void Create(string taskName, ITaskHandler handler)
        {
            _handlers.TryAdd(taskName, handler);
        }
        /// <summary>
        /// 新增任务
        /// </summary>
        /// <param name="taskNames">任务标识列表。任务标识，应具体唯一性</param>
        public void Create(List<string> taskNames, ITaskHandler handler)
        {
            taskNames.ForEach(x =>
            {
                Create(x, handler);
            });
        }

        public List<string> GetAllTaskNames()
        {
            return _handlers.Keys.ToList();
        }

        /// <summary>
        /// 获取任务处理器
        /// </summary>
        /// <param name="taskName"></param>
        /// <returns></returns>
        public ITaskHandler GetHandler(string taskName)
        {
            _handlers.TryGetValue(taskName, out ITaskHandler handler);
            return handler;
        }
    }
}
