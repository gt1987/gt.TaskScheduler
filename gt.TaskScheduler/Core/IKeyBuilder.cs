﻿/**************************************************************
 * Copyright gt1987. All rights reserved.
 * 
 * Author: guitao(guitao@eastmoney.com) 
 * Create Date: 2020/5/10 14:22:11
 * Description: IKeyBuilder
 *          
 * Revision History:
 *      Date         Author               Description
 *              
***************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace gt.TaskScheduler.Core
{
    public interface IKeyBuilder
    {
        /// <summary>
        /// 获取Leader节点Key
        /// </summary>
        /// <returns></returns>
        string GetMemberLeaderKey();
        /// <summary>
        /// 获取节点列表key
        /// </summary>
        /// <returns></returns>
        string GetMemberCollectionKey();
        /// <summary>
        /// 获取任务队列Key
        /// </summary>
        /// <returns></returns>
        string GetTaskQueueKey();
        /// <summary>
        /// 获取集群锁Key
        /// </summary>
        /// <returns></returns>
        string GetLockKey();
    }
}
