﻿/**************************************************************
 * Copyright gt1987. All rights reserved.
 * 
 * Author: guitao(guitao@eastmoney.com) 
 * Create Date: 2020/5/23 16:08:34
 * Description: LogManager
 *          
 * Revision History:
 *      Date         Author               Description
 *              
***************************************************************/

using log4net;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace gt.TaskScheduler.Core
{
    internal sealed class LogHelper
    {
        private static ILog _logger= LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public static void SetLogger(ILog logger)
        {
            _logger = logger;
        }

        public static ILog GetLogger()
        {
            return _logger;
        }
    }
}
