﻿/**************************************************************
 * Copyright gt1987. All rights reserved.
 * 
 * Author: guitao(guitao@eastmoney.com) 
 * Create Date: 2020/5/9 14:43:26
 * Description: ITaskSchedulerUniqueIdGenerator
 *          
 * Revision History:
 *      Date         Author               Description
 *              
***************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace gt.TaskScheduler.Core
{
    /// <summary>
    /// 任务调度器 唯一标识 生成器
    /// 为每个节点的任务调度器 生成唯一标识。
    /// </summary>
    public interface ITaskSchedulerUniqueIdGenerator
    {
        /// <summary>
        /// 获取 任务调度器 唯一标识
        /// 该标识应当是稳定的、唯一的。
        /// </summary>
        /// <returns></returns>
        string GetUniqueName();
    }
}
