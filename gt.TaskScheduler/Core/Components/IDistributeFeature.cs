﻿/**************************************************************
 * Copyright gt1987. All rights reserved.
 * 
 * Author: guitao(guitao@eastmoney.com) 
 * Create Date: 2020/5/8 21:21:13
 * Description: IDistributeFeature
 *          
 * Revision History:
 *      Date         Author               Description
 *              
***************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace gt.TaskScheduler.Core.Components
{
    /// <summary>
    /// 分布式组件Feature
    /// </summary>
    public interface IDistributeFeature : IDisposable
    {
        /// <summary>
        /// 获取分布式锁
        /// </summary>
        /// <returns></returns>
        bool GetLock(string key, string lockValue);
        /// <summary>
        /// 释放锁
        /// </summary>
        bool ReleaseLock(string key, string lockValue);
        /// <summary>
        /// 设置或更新集群数据
        /// </summary>
        void SetData(string key, string field, string value);
        /// <summary>
        /// 获取集群数据
        /// </summary>
        string GetData(string key, string field);
        /// <summary>
        /// 获取某个key 对应的所有数据
        /// </summary>
        Dictionary<string, string> GetAllData(string key);
        /// <summary>
        /// 设置一个有序队列数据
        /// </summary>
        void Enqueue(string key, List<string> datas);
        /// <summary>
        /// 获取并移除有序队列的下一个数据
        /// </summary>
        string Dequeue(string key);
        /// <summary>
        /// 删除集群数据
        /// </summary>
        void DeleteData(string key);
        /// <summary>
        /// 删除集群数据
        /// </summary>
        void DeleteData(string key, string field);
    }
}
