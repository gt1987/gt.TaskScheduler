﻿/**************************************************************
 * Copyright gt1987. All rights reserved.
 * 
 * Author: guitao(guitao@eastmoney.com) 
 * Create Date: 2020/5/9 13:09:55
 * Description: TaskSchedulerOptions
 *          
 * Revision History:
 *      Date         Author               Description
 *              
***************************************************************/

using gt.TaskScheduler.Core;
using gt.TaskScheduler.Core.Components;
using log4net;
using log4net.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace gt.TaskScheduler
{
    public class TaskSchedulerOptions
    {
        /// <summary>
        /// 集群Name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 启用线程数，并行处理task
        /// </summary>
        public int Threads { get; set; }
        /// <summary>
        /// 分布式中间件
        /// </summary>
        public IDistributeFeature DistributeFeature { get; set; }
        /// <summary>
        /// 节点唯一标识生成器
        /// </summary>
        public ITaskSchedulerUniqueIdGenerator UniqueIdGenerator { get; set; }
        /// <summary>
        /// KeyBuilder,生成相关Key标识
        /// </summary>
        public IKeyBuilder KeyBuilder { get; set; }
        /// <summary>
        /// TaskHandler 工厂，用于存储task与Handler 映射关系
        /// </summary>
        public ITaskHandlerProvider HandlerProvider { get; set; }
        /// <summary>
        /// Logger from log4net
        /// </summary>
        public ILog Logger { get; set; }
    }
}
