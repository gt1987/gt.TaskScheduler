using gt.TaskScheduler.Core.Components;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace gt.TaskScheduler.Test
{
    public class RedisFeatureTest : IDisposable
    {
        private readonly IDistributeFeature _distribute;

        public RedisFeatureTest()
        {
            _distribute = new RedisDistributeFeature("127.0.01:6379", 3, 10);
        }

        [Fact]
        public void LockTest()
        {
            var lockKey = "ts:test:lock";
            var result1 = Task.Run(() =>
             {
                 var lockValue = Guid.NewGuid().ToString("N");
                 var flag = false;
                 if (_distribute.GetLock(lockKey, lockValue))
                 {
                     Task.Delay(1000).Wait();
                     flag = true;
                     _distribute.ReleaseLock(lockKey, lockValue);
                 }
                 return flag;
             });
            var result2 = Task.Run(() =>
            {
                var lockValue = Guid.NewGuid().ToString("N");
                var flag = false;
                if (_distribute.GetLock(lockKey, lockValue))
                {
                    Task.Delay(1000).Wait();
                    flag = true;
                    _distribute.ReleaseLock(lockKey, lockValue);
                }
                return flag;
            });

            Assert.NotEqual(result1.Result, result2.Result);
        }
        [Fact]
        public void LockExpiredTest()
        {
            var lockKey = "ts:test:lock2";
            var result1 = Task.Run(() =>
            {
                var lockValue = Guid.NewGuid().ToString("N");
                var flag = false;
                if (_distribute.GetLock(lockKey, lockValue))
                {
                    Task.Delay(1000).Wait();
                    flag = true;
                    //_distribute.ReleaseLock(lockKey, lockValue);
                }
                return flag;
            });

            Assert.True(result1.Result);

            var result2 = Task.Run(() =>
            {
                var lockValue = Guid.NewGuid().ToString("N");
                var flag = false;
                Task.Delay(4000).Wait();//expired
                if (_distribute.GetLock(lockKey, lockValue))
                {
                    Task.Delay(1000).Wait();
                    flag = true;
                    _distribute.ReleaseLock(lockKey, lockValue);
                }
                return flag;
            });

            Assert.True(result2.Result);
        }
        [Fact]
        public void CRUDTest()
        {
            string key = "ts:test:h";
            int i = 0;
            while (i < 100)
            {
                _distribute.SetData(key, i.ToString(), i.ToString());
                i++;
            }
            Assert.NotNull(_distribute.GetAllData(key));
            Assert.Equal("10", _distribute.GetData(key, "10"));

            _distribute.SetData(key, "10", "10:10");
            Assert.Equal("10:10", _distribute.GetData(key, "10"));

            _distribute.DeleteData(key, "10");
            Assert.Null(_distribute.GetData(key, "10"));

            _distribute.DeleteData(key);
            Assert.NotNull(_distribute.GetAllData(key));
        }
        [Fact]
        public void QueueTest()
        {
            string key = "ts:test:l";
            int i = 0;
            List<string> list = new List<string>();
            while (i < 100)
            {
                list.Add(i.ToString());
                i++;
            }
            _distribute.Enqueue(key, list);

            while (i > 0)
            {
                Assert.NotNull(_distribute.Dequeue(key));
                i--;
            }

            _distribute.DeleteData(key);
        }

        public void Dispose()
        {
            _distribute.Dispose();
        }
    }
}
