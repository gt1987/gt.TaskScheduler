﻿/**************************************************************
 * Copyright gt1987. All rights reserved.
 * 
 * Author: guitao(guitao@eastmoney.com) 
 * Create Date: 2020/5/12 21:20:47
 * Description: TaskSchedulerTest
 *          
 * Revision History:
 *      Date         Author               Description
 *              
***************************************************************/

using gt.TaskScheduler.Core;
using gt.TaskScheduler.Core.Components;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace gt.TaskScheduler.Test
{
    public class TaskSchedulerTest
    {
        private const string _redisConn = "127.0.01:6379";
        private readonly string _sampleKey = "ts:test:sample:result";

        public TaskSchedulerTest()
        { }

        [Fact]
        public void DefaultBuildTest()
        {
            var schedulerName = "build_test";

            using (var distribute = new RedisDistributeFeature(_redisConn, 3, 10))
            {
                TaskSchedulerManagerBuilder builder = new TaskSchedulerManagerBuilder(schedulerName, distribute);
                builder.AddTaskHandler("sample1", new SampleHandler(distribute));
                builder.AddTaskHandler("sample2", new SampleHandler(distribute));
                var taskScheduler = builder.Build();
                taskScheduler.Run();

                Assert.Equal("0", distribute.GetData(_sampleKey, "sample1"));
                Assert.Equal("0", distribute.GetData(_sampleKey, "sample2"));

                distribute.DeleteData(_sampleKey);
            }
        }
    }

    public class SampleHandler : ITaskHandler
    {
        private IDistributeFeature _distribute;
        private readonly string _sampleKey = "ts:test:sample:result";
        public SampleHandler(IDistributeFeature distribute)
        {
            _distribute = distribute;
        }

        public Task Execute(string tag)
        {
            _distribute.SetData(_sampleKey, tag, "0");
            return Task.Delay(500);
        }
    }
}
